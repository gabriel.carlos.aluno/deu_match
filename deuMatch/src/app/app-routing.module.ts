import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path:"",
    pathMatch:"full",
    redirectTo:"home"
  },
  {
    path: "home",
    loadChildren: () => import("./components/home/home.module").then(m => m.HomeModule)
  },
  {
    path: "resultado-ruim",
    loadChildren: () => import("./components/resultado-ruim/resultado-ruim.module").then(m => m.ResultadoRuimModule)
  },
  {
    path: "resultado-mediano",
    loadChildren: () => import("./components/resultado-mediano/resultado-mediano.module").then(m => m.ResultadoMedianoModule)
  },
  {
    path: "resultado-maximo",
    loadChildren: () => import("./components/resultado-maximo/resultado-maximo.module").then(m => m.ResultadoMaximoModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
