import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ResultadoService {

  private readonly URL="https://3000-gabrielcarlosa-deumatch-y9dceyctebt.ws-us88.gitpod.io/"

  constructor( private http : HttpClient
    ) { }

    calcularMatch(): Observable<any>{
      return this.http.get<any>(`${this.URL}resultado`)
    }
}
