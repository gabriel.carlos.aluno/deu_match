import { Injectable } from '@angular/core';
import { Dados } from 'src/app/models/Dados';

@Injectable({
  providedIn: 'root'
})
export class DadosService {
  private _dados: Dados = new Dados;
  
    public set dados(dados:Dados){
      this._dados=dados
      console.log("dados service: " + this._dados.nome1, this._dados.nome2,this._dados.porcentagem)
      console.log("dados do set" + this._dados)
    }

    public get dados(){
    return this._dados;
    }
}
