import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Dados } from 'src/app/models/Dados';
import { DadosService } from '../services/dados-service/dados.service';
import { ResultadoService } from '../services/resultado-service/resultado.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [DadosService]
})
export class HomeComponent {

  dados:Dados

  constructor(
    private resultadoService: ResultadoService,
    private router: Router,
    private dadosServices: DadosService
  ) { 
    this.dados= new Dados()
  }

  calcularMatch():void{
    this.resultadoService.calcularMatch()
    .subscribe({
      next: (resposta) => {
        console.log(resposta)
        this.dados.porcentagem=resposta.result  
        console.log("home: "+this.dados.nome1, this.dados.nome2, this.dados.porcentagem)
        this.dadosServices.dados=this.dados
        this.router.navigate(['/', resposta.mensagem])
      },
      error: (erro) => {
        console.error(erro)
      }
    })
  }
}
