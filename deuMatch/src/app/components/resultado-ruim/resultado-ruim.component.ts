import { Component, OnInit } from '@angular/core';
import { Dados } from 'src/app/models/Dados';
import { DadosService } from '../services/dados-service/dados.service';

@Component({
  selector: 'app-resultado-ruim',
  templateUrl: './resultado-ruim.component.html',
  styleUrls: ['./resultado-ruim.component.css'],
  providers: [DadosService]
})
export class ResultadoRuimComponent implements OnInit {
 
  dados:Dados
  constructor(
    private dadosService: DadosService
){
  this.dados=new Dados()
}
  ngOnInit(): void {
    console.log(this.dadosService.dados)
    this.dados=this.dadosService.dados
  }

}
