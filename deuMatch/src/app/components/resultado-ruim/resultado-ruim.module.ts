import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ResultadoRuimRoutingModule } from './resultado-ruim-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ResultadoRuimRoutingModule
  ]
})
export class ResultadoRuimModule { }
