import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResultadoRuimComponent } from './resultado-ruim.component';

describe('ResultadoRuimComponent', () => {
  let component: ResultadoRuimComponent;
  let fixture: ComponentFixture<ResultadoRuimComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResultadoRuimComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ResultadoRuimComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
