import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ResultadoRuimComponent } from './resultado-ruim.component';

const routes: Routes = [
  {
    path:"",
    component: ResultadoRuimComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ResultadoRuimRoutingModule { }
