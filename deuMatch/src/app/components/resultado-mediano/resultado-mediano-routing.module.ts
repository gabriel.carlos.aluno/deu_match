import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ResultadoMedianoComponent } from './resultado-mediano.component';

const routes: Routes = [
  {
    path:"",
    component: ResultadoMedianoComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ResultadoMedianoRoutingModule { }
