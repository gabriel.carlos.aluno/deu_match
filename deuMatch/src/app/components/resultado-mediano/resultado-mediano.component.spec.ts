import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResultadoMedianoComponent } from './resultado-mediano.component';

describe('ResultadoMedianoComponent', () => {
  let component: ResultadoMedianoComponent;
  let fixture: ComponentFixture<ResultadoMedianoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResultadoMedianoComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ResultadoMedianoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
