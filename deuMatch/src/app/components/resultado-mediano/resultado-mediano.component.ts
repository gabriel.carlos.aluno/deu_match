import { Component, OnInit } from '@angular/core';
import { Dados } from 'src/app/models/Dados';
import { DadosService } from '../services/dados-service/dados.service';

@Component({
  selector: 'app-resultado-mediano',
  templateUrl: './resultado-mediano.component.html',
  styleUrls: ['./resultado-mediano.component.css']
})
export class ResultadoMedianoComponent implements OnInit{
  dados:Dados
  constructor(
    private dadosService: DadosService
){
  this.dados=new Dados()
}
  ngOnInit(): void {
    console.log(this.dadosService.dados)
    this.dados=this.dadosService.dados
  }

}
