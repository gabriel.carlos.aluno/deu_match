import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ResultadoMedianoRoutingModule } from './resultado-mediano-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ResultadoMedianoRoutingModule
  ]
})
export class ResultadoMedianoModule { }
