import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResultadoMaximoComponent } from './resultado-maximo.component';

describe('ResultadoMaximoComponent', () => {
  let component: ResultadoMaximoComponent;
  let fixture: ComponentFixture<ResultadoMaximoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResultadoMaximoComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ResultadoMaximoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
