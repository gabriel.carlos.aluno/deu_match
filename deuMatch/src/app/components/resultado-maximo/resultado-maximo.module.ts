import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ResultadoMaximoRoutingModule } from './resultado-maximo-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ResultadoMaximoRoutingModule
  ]
})
export class ResultadoMaximoModule { }
