import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ResultadoMaximoComponent } from './resultado-maximo.component';

const routes: Routes = [{
  path:"",
  component: ResultadoMaximoComponent
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ResultadoMaximoRoutingModule { }
