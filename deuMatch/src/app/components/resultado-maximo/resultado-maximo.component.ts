import { Component, OnInit } from '@angular/core';
import { Dados } from 'src/app/models/Dados';
import { DadosService } from '../services/dados-service/dados.service';

@Component({
  selector: 'app-resultado-maximo',
  templateUrl: './resultado-maximo.component.html',
  styleUrls: ['./resultado-maximo.component.css']
})
export class ResultadoMaximoComponent implements OnInit{
  dados:Dados
  constructor(
    private dadosService: DadosService
){
  this.dados=new Dados()
}
  ngOnInit(): void {
    console.log(this.dadosService.dados)
    this.dados=this.dadosService.dados
  }
}
