import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ResultadoRuimComponent } from './components/resultado-ruim/resultado-ruim.component';
import { HeaderComponent } from './components/common/header/header.component';
import { FooterComponent } from './components/common/footer/footer.component';
import { ResultadoMedianoComponent } from './components/resultado-mediano/resultado-mediano.component';
import { ResultadoMaximoComponent } from './components/resultado-maximo/resultado-maximo.component';
import { HomeComponent } from './components/home/home.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { DadosService } from './components/services/dados-service/dados.service';

@NgModule({
  declarations: [
    AppComponent,
    ResultadoRuimComponent,
    HeaderComponent,
    FooterComponent,
    ResultadoMedianoComponent,
    ResultadoMaximoComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [DadosService],
  bootstrap: [AppComponent]
})
export class AppModule { }
