const { Router } = require('express')
const routes = Router()
const DeuMatchController = require('../controllers/deuMatchController')

Router.get('/deumatch/:nomeA/:nomeB', DeuMatchController.verificarMatch)
Router.post('/deuMatch', DeuMatchController.verificarMatch)

routes.get('/resultado', (req, res) =>{
    const calcMatch = Math.floor(Math.random() * 101);

    if (calcMatch <= 50) {
        return res.status(200).json({
            "mensagem": "resultado-ruim",
            "result": calcMatch
        })
    }

    if (calcMatch <= 75) {
        return res.status(200).json({
            "mensagem": "resultado-mediano",
            "result": calcMatch
        })
    }

    if (calcMatch <= 100) {
        return res.status(200).json({
            "mensagem": "resultado-maximo",
            "result": calcMatch
        })
    }
})

routes.post('/resultado', (req, res) =>{
const resultado = req.body
const calcMatch = Math.floor(Math.random() * 101);

    if (calcMatch <= 50) {
        return res.status(200).json({
            "mensagem": "resultado-ruim",
            "result": calcMatch
        })
    }

    if (calcMatch <= 75) {
        return res.status(200).json({
            "mensagem": "resultado-mediano",
            "result": calcMatch
        })
    }

    if (calcMatch <= 100) {
        return res.status(200).json({
            "mensagem": "resultado-maximo",
            "result": calcMatch
        })
    }
})

 module.exports = routes
