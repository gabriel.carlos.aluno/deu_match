const bodyParser = require('body-parser')
const resultado = require('./resultadoRoute')
const cors = require('cors')

module.exports = (server) => {
    server.use((req, res, next) => {
        res.header("Access-Control-Allow-Origin", "*")
        res.header("Access-Control-Allow-Methods", "GET, POST")
        res.header("Access-Control-Allow-Headers", "Content-Type")
        server.use(cors())
        next()
    })

    server.use(bodyParser.json())
    server.use(resultado)
}