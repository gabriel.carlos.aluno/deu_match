class DeuMatchController{
        static async randomizarMatch() {
            return Math.round(Math.random() * 100)
        }

        static async verificarMatch(req, res) {
            let nomeA = ""
            let nomeB = ""

            if (req.params.nomeA !== undefined && req.params.nomeB !== undefined){
                nomeA = req.params.nomeA
                nomeB = req.params.nomeB
            } else if (req.body.nomeA !== undefined && req.body.nomeB !== undefined) {
                nomeA = req.body.                                  
                nomeB = req.body.nomeB
            } else {
                return res.status(400).json({
                    mensagem: "Não foram encontrados os parâmetros necessários para a requisição"
                })
            }

            let mensagem = ""
            let valorMatch = await DeuMatchController.randomizarMatch()

            if (valorMatch<50){
                mensagem =`Não foi dessa vez que ${nomeA} e ${nomeB} ficarão juntos...`
            }else if(valorMatch<75){
                mensagem=`O cenário não é perfeito, mas ${nomeA} e ${nomeB} tem boas chances de dar certo.`
            }else {
                mensagem =`${nomeA} e ${nomeB} foram feitos um para outro!`
            }

        return res.status(200).json({
            mensagem:mensagem,
            match: valorMatch
        })
    }
}     

module.exports = DeuMatchController